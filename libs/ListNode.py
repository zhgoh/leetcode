""" List Node """


class ListNode:
    """
    ListNode simulates a nodes in a linked list
    """
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

    def __str__(self):
        """
        Print the nodes
        """
        res = ""
        curr = self
        while curr is not None:
            res += curr.val
            res += ", "
            curr = curr.next
        return res

    def print_nodes(self):
        """
        Print the nodes
        """
        curr = self
        while curr is not None:
            print(curr.val)
            curr = curr.next
        print()


def make_node(ls: list) -> ListNode:
    """
    Make a ListNode from linked list
    """
    head = ListNode()
    res = head
    for num in ls:
        res.next = ListNode(num)
        res = res.next
    return head.next


def get_len(node: ListNode) -> int:
    """
    Get the length of nodes
    """
    idx = 0
    while node:
        idx += 1
        node = node.next
    return idx
