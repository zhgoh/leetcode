#!/usr/bin/env python3
""" Main file to run all exercises """

# Fifty questions from Tech Interview Handbook

# Week 1 - Sequences
# from fifty.week1.two_sum import run
# from fifty.week1.contains_duplicate import run
# from fifty.week1.best_time_to_buy_stock import run
# from fifty.week1.valid_anagram import run
# from fifty.week1.valid_parentheses import run
# from fifty.week1.maximum_subarray import run
# from fifty.week1.product_of_array_except_self import run
# from fifty.week1.three_sum import run
# from fifty.week1.merge_intervals import run
# from fifty.week1.group_anagram import run
# from fifty.week1.maximum_product_subarray import run
# from fifty.week1.search_in_rotated_sorted_array import run

# Week 2 - Data structures
# from fifty.week2.reverse_linked_list import run
# from fifty.week2.linked_list_cycle import run
# from fifty.week2.container_with_most_water import run
# from fifty.week2.find_minimum_in_rotated_sorted_array import run
# from fifty.week2.longest_repeating_character_replacement import run
# from fifty.week2.longest_substring_without_repeating_characters import run
# from fifty.week2.number_of_islands import run
# from fifty.week2.remove_nth_node_from_list import run
# from fifty.week2.palindrome_substrings import run
# from fifty.week2.pacific_atlantic_water_flow import run
# from fifty.week2.minimum_window_substring import run

# Week 3 - Non-linear data structures
from fifty.week3.invert_binary_tree import run

# Extra questions from leetcode
# from extras.add_two_numbers import run
# from extras.longest_palindrome_substring import run
# from extras.zigzag_conversion import run
# from extras.reverse_integer import run
# from extras.string_to_integer import run
# from extras.palindrome_number import run
# from extras.regular_expression_matching import run
# from extras.roman_to_integer import run

run()
