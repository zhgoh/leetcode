#!/usr/bin/env python3
""" Valid parentheses """


class Solution:
    """
    Solution
    """
    @staticmethod
    def is_valid_stack(string: str) -> bool:
        """
        Is valid using stack
        """
        symbols = []
        lookup = {"]": "[", "}": "{", ")": "("}
        for char in string:
            if char in ("(", "{", "["):
                symbols.append(char)
            else:
                if len(symbols) > 0:
                    if symbols[-1] == lookup[char]:
                        symbols.pop()
                    else:
                        return False
                else:
                    return False
        return len(symbols) == 0


def run():
    """
    Run tests
    """
    assert Solution.is_valid_stack("()")
    assert Solution.is_valid_stack("()[]{}")
    assert Solution.is_valid_stack("([{}])")
    assert not Solution.is_valid_stack("(]")
    assert not Solution.is_valid_stack("}]")
    assert not Solution.is_valid_stack("]]")
    assert not Solution.is_valid_stack("[{")
    assert not Solution.is_valid_stack("([)")
    assert not Solution.is_valid_stack("(])")
    assert not Solution.is_valid_stack("[")
    assert not Solution.is_valid_stack("([)]")
    print("Valid parentheses done")
