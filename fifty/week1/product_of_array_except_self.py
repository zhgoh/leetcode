#!/usr/bin/env python3
""" Product of array except self """
import functools
import itertools
import operator


class Solution:
    """
    Solution
    """

    @staticmethod
    def product_except_self_div(nums: list[int]) -> list[int]:
        """
        Product except self using prefix, suffix products
        https://leetcode.com/problems/product-of-array-except-self/discuss/1597994/C%2B%2B-3-Simple-Solutions-w-Explanation-or-Prefix-and-Suffix-product-O(1)-space-approach
        """
        # 1. if 1 zero
        # 2. if many zero
        # 3. Normal case
        prod, zero_cnt = functools.reduce(
            lambda x, y: x * (y if y else 1),
            nums, 1), nums.count(0)
        if zero_cnt > 1:
            return [0] * len(nums)
        for i, val in enumerate(nums):
            if zero_cnt:
                nums[i] = 0 if val else prod
            else:
                nums[i] = prod // val
        return nums

    @staticmethod
    def product_except_self_prefix(nums: list[int]) -> list[int]:
        """
        Product except self, using division
        """
        pre_arr = list(itertools.accumulate(nums, operator.mul))
        suf_arr = list(itertools.accumulate(nums[::-1], operator.mul))[::-1]
        nums_len = len(nums)

        results = []
        for i in range(nums_len):
            prefix = 1 if i - 1 < 0 else pre_arr[i - 1]
            suffix = 1 if i + 1 >= nums_len else suf_arr[i + 1]
            results.append(prefix * suffix)
        return results


def run():
    """
    Run tests
    """
    assert Solution.product_except_self_div([1, 2, 3, 4]) == [24, 12, 8, 6]
    assert Solution.product_except_self_div([1, 4, 0]) == [0, 0, 4]
    assert Solution.product_except_self_div([0, 2, 1, 4, 0]) == [0, 0, 0, 0, 0]

    assert Solution.product_except_self_prefix([1, 2, 3, 4]) == [24, 12, 8, 6]
    assert Solution.product_except_self_prefix([1, 4, 0]) == [0, 0, 4]
    assert Solution.product_except_self_prefix(
        [0, 2, 1, 4, 0]) == [0, 0, 0, 0, 0]
    print("Product of array except self done")
