#!/usr/bin/env python3
""" Valid anagram """


class Solution:
    """
    Solution
    """
    @staticmethod
    def is_anagram(string: str, target: str) -> bool:
        """
        Is an anagram
        """
        str_len = len(string)
        tgt_len = len(target)

        if str_len != tgt_len:
            return False

        counts = {}
        for char in string:
            if char not in counts:
                counts[char] = 0
            counts[char] += 1

        for char in target:
            if char not in counts:
                return False
            counts[char] -= 1
            if counts[char] < 0:
                return False

        return True


def run():
    """
    Run test
    """
    assert Solution.is_anagram("anagram", "nagaram")
    assert not Solution.is_anagram("rat", "car")
    assert not Solution.is_anagram("man", "amen")
    assert not Solution.is_anagram("man", "men")
    assert not Solution.is_anagram("aacc", "ccac")
    print("Valid anagram done")
