#!/usr/bin/env python3
""" Best time to buy stock """


class Solution:
    """
    Solutions
    """

    @staticmethod
    def max_profit_slow(prices: list[int]) -> int:
        """
        Get max profit
        """
        prices_len = len(prices)
        if prices_len == 0:
            return 0

        max_profit = 0
        for i in range(prices_len):
            for j in range(i + 1, prices_len):
                max_profit = max(max_profit, prices[j] - prices[i])
        return max_profit

    @staticmethod
    def max_profit_fast(prices: list[int]) -> int:
        """
        Get max profit faster
        """
        prices_len = len(prices)
        if prices_len < 2:
            return 0

        left, right = 0, 1
        max_profit = 0

        while right < prices_len:
            max_profit = max(max_profit, prices[right] - prices[left])

            if prices[right] < prices[left]:
                left = right
                right += 1
            elif prices[left] <= prices[right]:
                right += 1
        return max_profit


def run():
    """
    Run tests
    """
    assert Solution.max_profit_slow([7, 1, 5, 3, 6, 4]) == 5
    assert Solution.max_profit_slow([7, 6, 4, 3, 1]) == 0
    assert Solution.max_profit_slow([2, 4, 1]) == 2
    assert Solution.max_profit_slow([2, 1, 4]) == 3

    assert Solution.max_profit_fast([7, 1, 5, 3, 6, 4]) == 5
    assert Solution.max_profit_fast([7, 6, 4, 3, 1]) == 0
    assert Solution.max_profit_fast([2, 4, 1]) == 2
    assert Solution.max_profit_fast([2, 1, 4]) == 3
    assert Solution.max_profit_fast([2, 1, 2, 0, 1]) == 1
    assert Solution.max_profit_fast([3, 3]) == 0
    assert Solution.max_profit_fast([2, 1, 2, 1, 0, 1, 2]) == 2
    assert Solution.max_profit_fast([2, 4, 1, 11, 7]) == 10
    print("Done")
