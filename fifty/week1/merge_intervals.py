#!/usr/bin/env python3

class Solution:
    """
    Solution
    """
    @staticmethod
    def merge(intervals: list[list[int]]) -> list[list[int]]:
        """
        Merge
        """
        intervals.sort(key=lambda x: x[0])
        interval_len = len(intervals)
        res = [intervals[0]]
        for i in range(1, interval_len):
            if intervals[i][0] <= res[-1][1]:
                res[-1][1] = max(intervals[i][1], res[-1][1])
            else:
                res.append(intervals[i])

        return res


def run():
    """
    Run tests
    """
    assert Solution.merge([[1, 4], [4, 7], [7, 8]]) == [[1, 8]]
    assert Solution.merge([[1, 3], [4, 7], [7, 8]]) == [[1, 3], [4, 8]]
    assert Solution.merge([[1, 2], [3, 7], [8, 9]]) == [[1, 2], [3, 7], [8, 9]]
    assert Solution.merge([[4, 5], [1, 2], [8, 9]]) == [[1, 2], [4, 5], [8, 9]]
    assert Solution.merge([[0, 0], [0, 0], [0, 0]]) == [[0, 0]]
    assert Solution.merge([[1, 4], [2, 3]]) == [[1, 4]]
    assert Solution.merge([[2, 3], [4, 5], [6, 7], [8, 9], [1, 10]]) == [[1, 10]]
    print("Merge intervals done")
