#!/usr/bin/env python3
""" Group Anagram """


class Solution:
    """
    Solution
    """
    @staticmethod
    def group_anagrams(strings: list[str]) -> list[list[str]]:
        """
        Group anagrams together
        """
        anagrams = {}
        for string in strings:
            key = "".join(sorted(string))
            if key not in anagrams:
                anagrams[key] = [string]
            else:
                anagrams[key] += [string]
        return list(anagrams.values())


def run():
    """
    Tests
    """
    assert (
        Solution.group_anagrams(
            ["eat", "tea", "tan", "ate", "nat", "bat"]) ==
        [['eat', 'tea', 'ate'], ['tan', 'nat'], ['bat']]
    )
    assert Solution.group_anagrams([""]) == [[""]]
    assert Solution.group_anagrams(["a"]) == [["a"]]
    print("Group anagram done")
