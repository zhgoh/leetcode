#!/usr/bin/env python3
""" Two sums leetcode """


class Solution:
    """
    Solution
    """
    @staticmethod
    def two_sum(nums: list[int], target: int) -> list[int]:
        """
        Find the index of the two number that sums up to the
        target

        Time: O(n2)
        Storage: O(1)
        """
        nums_len = len(nums)
        for i in range(nums_len):
            first = nums[i]
            for j in range(nums_len):
                second = nums[j]

                if i == j:
                    # Cannot repeat
                    continue

                if (first + second) == target:
                    return [i, j]

        return []

    @staticmethod
    def two_sum_fast(nums: list[int], target: int) -> list[int]:
        """
        Find the index of the two number that sums up to the
        target.

        Sort the list in descending order
        Pick max, min

        https://www.youtube.com/watch?v=KLlXCFG5TnA

        Time: O(n)
        Storage: O(n)
        """
        diff_map = {}

        i = 0
        res: list[int] = []
        for elem in nums:
            diff = target - elem

            if diff not in diff_map:
                diff_map[elem] = i
            else:
                return [diff_map[diff], i]
            i += 1
        return res


def run():
    """
    Run the tests
    """
    assert Solution.two_sum([2, 7, 11, 15], 9) == [0, 1]
    assert Solution.two_sum([3, 2, 4], 6) == [1, 2]
    assert Solution.two_sum_fast([2, 7, 11, 15], 9) == [0, 1]
    assert Solution.two_sum_fast([3, 2, 4], 6) == [1, 2]
    print("Done")
