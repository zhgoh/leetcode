#!/usr/bin/env python3
""" Maximum subarray """


class Solution:
    """
    Solution
    """
    # Time: O(n2)
    @staticmethod
    def max_sub_array(nums: list[int]) -> int:
        """
        Max sub array
        """
        nums_len = len(nums)
        max_val = nums[0]
        for i in range(nums_len):
            val = 0
            for j in range(i, nums_len):
                val += nums[j]
                max_val = max(max_val, val)
        return max_val

    @staticmethod
    def max_sub_array_fast(nums: list[int]) -> int:
        """ Max sub array """
        nums_len = len(nums)
        max_val = nums[0]
        current = 0

        for i in range(nums_len):
            current = max(current, 0)
            current += nums[i]
            max_val = max(max_val, current)
        return max_val


def run():
    """
    Run tests
    """
    assert Solution.max_sub_array([-2, 1, -3, 4, -1, 2, 1, -5, 4]) == 6
    assert Solution.max_sub_array([1]) == 1
    assert Solution.max_sub_array([5, 4, -1, 7, 8]) == 23

    assert Solution.max_sub_array_fast([-2, 1, -3, 4, -1, 2, 1, -5, 4]) == 6
    assert Solution.max_sub_array_fast([1]) == 1
    assert Solution.max_sub_array_fast([5, 4, -1, 7, 8]) == 23
    print("Max sub array done")
