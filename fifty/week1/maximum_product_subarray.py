#!/usr/bin/env python3
""" Maximum product subarray """


class Solution:
    """
    Solution
    """
    @staticmethod
    def max_product(nums: list[int]) -> int:
        """
        Get Max product sub array
        """
        res = max(nums)
        min_val, max_val = 1, 1

        for num in nums:
            if num == 0:
                min_val, max_val = 1, 1
                continue

            old_max = max_val * num
            max_val = max(num * max_val, num * min_val, num)
            min_val = min(old_max, num * min_val, num)
            res = max(res, max_val)
        return res


def run():
    """
    Run
    """
    assert(Solution.max_product([2, 3, -2, 4])) == 6
    assert(Solution.max_product([2, 3, 2, -1])) == 12
    assert(Solution.max_product([-1])) == -1
    assert(Solution.max_product([-2, 0, -1])) == 0
    assert(Solution.max_product([-1, 0])) == 0
    assert(Solution.max_product([1, 0])) == 1
    assert(Solution.max_product([0, 2])) == 2
    assert(Solution.max_product([0, 0, 0])) == 0
    assert(Solution.max_product([-1, -1, -1])) == 1
    assert(Solution.max_product([0, -2, 0])) == 0
    assert(Solution.max_product([5, -2, -1])) == 10
    assert(Solution.max_product([2, -5, -2, -4, 3])) == 24
    print("Max product subarray done")
