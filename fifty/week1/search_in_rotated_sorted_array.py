#!/usr/bin/env python3
""" Search in rotated sorted array """


class Solution:
    """
    Solution
    """
    @staticmethod
    def search(nums: list[int], target: int) -> int:
        """
        Search for items
        """
        left, right = 0, len(nums) - 1
        while left <= right:
            mid = (left + right) // 2
            if target == nums[mid]:
                return mid

            if nums[left] <= nums[mid]:
                # Left sorted portion
                if target > nums[mid] or target < nums[left]:
                    left = mid + 1
                else:
                    right = mid - 1
            else:
                # Right sorted portion
                if target < nums[mid] or target > nums[right]:
                    right = mid - 1
                else:
                    left = mid + 1
        return -1


def run():
    """
    Run tests
    """
    # assert Solution.search([4, 5, 6, 7, 0, 1, 2], 0) == 4
    assert Solution.search([6, 0, 1, 2, 3, 4, 5], 0) == 1
    print("Search in rotated done")
