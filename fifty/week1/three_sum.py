#!/usr/bin/env python3
""" 3 Sum """


class Solution:
    """
    Solution
    """
    @staticmethod
    def three_sum(nums: list[int]) -> list[list[int]]:
        """
        Three sums
        """
        num_len = len(nums)

        res = []
        for i in range(num_len):
            diff_map = {}
            target = -nums[i]
            # Find the two sums for num
            for j in range(num_len):
                if i == j:
                    continue

                diff = target - nums[j]

                if diff not in diff_map:
                    diff_map[nums[j]] = 1
                else:
                    sums = [-target, nums[j], diff]
                    sums.sort()
                    if sums not in res:
                        res.append(sums)
        return res

    @staticmethod
    def three_sum_fast(nums: list[int]) -> list[list[int]]:
        """
        three sums fast
        """
        nums.sort()
        num_len = len(nums)

        res = []
        for i in range(num_len):
            a = nums[i]
            if i > 0 and a == nums[i - 1]:
                continue

            left, right = i + 1, num_len - 1
            while left < right:
                sums = a + nums[left] + nums[right]
                if sums < 0:
                    left += 1
                elif sums > 0:
                    right -= 1
                else:
                    res.append([a, nums[left], nums[right]])
                    left += 1
                    while nums[left] == nums[left - 1] and left < right:
                        left += 1
        return res


def run():
    """
    Run tests
    """
    assert Solution.three_sum(
        [-1, 0, 1, 2, -1, -4]) == [[-1, 0, 1], [-1, -1, 2]]

    assert Solution.three_sum_fast(
        [-1, 0, 1, 2, -1, -4]) == [[-1, -1, 2], [-1, 0, 1]]
