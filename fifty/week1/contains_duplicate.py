#!/usr/bin/env python3

class Solution:
    """
    Solution
    """
    @staticmethod
    def contains_duplicate(nums: list[int]) -> bool:
        """
        Contains duplicate
        """
        count = {}
        for num in nums:
            if num not in count:
                count[num] = 0
            count[num] += 1

        for val in count:
            if count[val] > 1:
                return True
        return False

    @staticmethod
    def contains_duplicate_sort(nums: list[int]) -> bool:
        nums.sort()
        prev = nums[0]
        for n in range(1, len(nums)):
            if nums[n] == prev:
                return True
            prev = nums[n]
        return False


def run():
    """
    Run tests
    """
    assert Solution.contains_duplicate([1, 2, 3, 1])
    assert Solution.contains_duplicate([1, 1, 1, 3, 3, 4, 3, 2, 4, 2])
    assert not Solution.contains_duplicate([1, 2, 3, 4])

    assert Solution.contains_duplicate_sort([1, 2, 3, 1])
    assert Solution.contains_duplicate_sort([1, 1, 1, 3, 3, 4, 3, 2, 4, 2])
    assert not Solution.contains_duplicate_sort([1, 2, 3, 4])
    print("Done")
