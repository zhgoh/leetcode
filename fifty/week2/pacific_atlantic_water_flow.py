#!/usr/bin/env python3
""" Maximum product subarray """


class Solution:
    """
    Solution
    """
    @staticmethod
    def dfs(heights: list[list[int]], row: int, col: int) -> bool:
        """
        DFS
        """
        visit: list[tuple[int, int]] = [(row, col)]
        explored: set[tuple[int, int]] = set()

        rows = len(heights) - 1
        cols = len(heights[0]) - 1

        # Visit all nodes to see if can reach both river
        atlantic = False
        pacific = False

        while len(visit) > 0 and not (atlantic and pacific):
            node = visit.pop()
            if node not in explored:
                row = node[0]
                col = node[1]
                explored.add(node)

                # if not pacific and (node[0] == 0 or node[1] == 0):
                if not pacific and (row == 0 or col == 0):
                    pacific = True

                # if not atlantic and (node[0] == rows or node[1] == cols):
                if not atlantic and (row == rows or col == cols):
                    atlantic = True

                directions = ((0, 1), (0, -1), (1, 0), (-1, 0))
                for direction in directions:
                    new_row = row + direction[0]
                    new_col = col + direction[1]

                    if (new_row, new_col) in explored:
                        continue

                    if 0 <= new_row <= rows and 0 <= new_col <= cols:
                        if heights[new_row][new_col] <= heights[row][col]:
                            visit.append((new_row, new_col))
        return atlantic and pacific

    @staticmethod
    def pacific_atlantic(heights: list[list[int]]) -> list[list[int]]:
        """
        Pacific atlantic

        Find if any cells can reach both pacific and atlantic ocean
        """
        rows = len(heights)
        cols = len(heights[0])

        res: list[list[int]] = []
        for row in range(rows):
            for col in range(cols):
                if Solution.dfs(heights, row, col):
                    res.append([row, col])
        return res

    @staticmethod
    def dfs_p(heights: list[list[int]],
              row: int, col: int, visit: set[tuple[int, int]], prev_height: int) -> None:
        """
        DFS that returns paths
        """
        rows = len(heights)
        cols = len(heights[0])

        if ((row, col) in visit or
            row < 0 or col < 0 or row == rows or col == cols or
            heights[row][col] < prev_height):
            return

        visit.add((row, col))

        directions = ((0, 1), (0, -1), (1, 0), (-1, 0))
        for direction in directions:
            Solution.dfs_p(heights, row + direction[0], col + direction[1], visit, heights[row][col])

    @staticmethod
    def pacific_atlantic_fast(heights: list[list[int]]) -> list[list[int]]:
        """
        Faster way to implement the search
        """
        rows = len(heights)
        cols = len(heights[0])

        atlantic, pacific = set(), set()

        for col in range(cols):
            Solution.dfs_p(heights, 0, col, pacific, heights[0][col])
            Solution.dfs_p(heights, rows - 1, col, atlantic, heights[rows - 1][col])

        for row in range(rows):
            Solution.dfs_p(heights, row, 0, pacific, heights[row][0])
            Solution.dfs_p(heights, row, cols - 1, atlantic, heights[row][cols - 1])

        res = []
        for row in range(rows):
            for col in range(cols):
                if (row, col) in pacific and (row, col) in atlantic:
                    res.append([row, col])
        return res



def compare(list_1: list, list_2: list) -> bool:
    """
    Compare two lists
    """

    print(list_1)
    print(list_2)

    if len(list_1) != len(list_2):
        return False

    list_1.sort()
    list_2.sort()

    for item in zip(list_1, list_2):
        if item[0] != item[1]:
            return False
    return True


def run():
    """
    Run
    """
    # print(Solution.dfs(
    #     [[1, 2, 2, 3, 5],
    #      [3, 2, 3, 4, 4],
    #      [2, 4, 5, 3, 1],
    #      [6, 7, 1, 4, 5],
    #      [5, 1, 1, 2, 4]], 3, 3))
    # print(Solution.dfs([[2, 1], [1, 2]], 0, 0))
    # print(Solution.dfs([[2, 1], [1, 2]], 0, 1))
    # print(Solution.dfs([[2, 1], [1, 2]], 1, 0))
    # print(Solution.dfs([[2, 1], [1, 2]], 1, 1))

    # print(Solution.dfs(
    #     [[1, 2, 2, 3, 5],
    #      [3, 2, 3, 4, 4],
    #      [2, 4, 5, 3, 1],
    #      [6, 7, 1, 4, 5],
    #      [5, 1, 1, 2, 4]], 2, 2))

    assert compare(
        Solution.pacific_atlantic(
            [[1, 2, 2, 3, 5],
             [3, 2, 3, 4, 4],
             [2, 4, 5, 3, 1],
             [6, 7, 1, 4, 5],
             [5, 1, 1, 2, 4]]), ([
                 [0, 4],
                 [1, 3],
                 [1, 4],
                 [2, 2],
                 [3, 0],
                 [3, 1],
                 [4, 0]]))

    assert compare(
        Solution.pacific_atlantic(
            [[2, 1], [1, 2]]), ([
                [0, 0],
                [0, 1],
                [1, 0],
                [1, 1]]))

    assert compare(
        Solution.pacific_atlantic_fast(
            [[1, 2, 2, 3, 5],
             [3, 2, 3, 4, 4],
             [2, 4, 5, 3, 1],
             [6, 7, 1, 4, 5],
             [5, 1, 1, 2, 4]]), ([
                 [0, 4],
                 [1, 3],
                 [1, 4],
                 [2, 2],
                 [3, 0],
                 [3, 1],
                 [4, 0]]))

    assert compare(
        Solution.pacific_atlantic_fast([[2, 1], [1, 2]]),
        [[0, 0],
         [0, 1],
         [1, 0],
         [1, 1]])

    print("Pacific atlantic water flow")
