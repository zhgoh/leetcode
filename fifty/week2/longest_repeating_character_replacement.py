#!/usr/bin/env python3
""" Longest repeating character replacement """


class Solution:
    """
    Solution
    """
    @staticmethod
    def character_replacement(string: str, k: int) -> int:
        """
        Character replacement
        """
        str_len = len(string)
        count: dict[str, int] = {}
        res = 0
        left = 0
        for right in range(str_len):
            count[string[right]] = 1 + count.get(string[right], 0)

            while (right - left + 1) - max(count.values()) > k:
                count[string[left]] -= 1
                left += 1
            res = max(res, right - left + 1)
        return res


def run():
    """
    Run
    """
    assert Solution.character_replacement("ABBAA", 2) == 5
    assert Solution.character_replacement("ABAB", 2) == 4
    assert Solution.character_replacement("AABABBA", 1) == 4
    assert Solution.character_replacement("AAAB", 0) == 3
    print("Longest repeating character replacement")
