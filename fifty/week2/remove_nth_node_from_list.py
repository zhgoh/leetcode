#!/usr/bin/env python3
""" Remove Nth node from End of List """
from typing import Optional
from libs.ListNode import ListNode, make_node


class Solution:
    """
    Solution
    """
    @staticmethod
    def remove_nth_from_end(
            head: Optional[ListNode],
            index: int) -> Optional[ListNode]:
        """
        Remove nth node from end of list
        """
        slow_ptr = head
        fast_ptr = head

        count = 0

        while fast_ptr:
            if count == index:
                break
            count += 1
            fast_ptr = fast_ptr.next

        if not fast_ptr:
            return head.next

        while fast_ptr:
            if fast_ptr.next is None:
                slow_ptr.next = slow_ptr.next.next
                break

            slow_ptr = slow_ptr.next
            fast_ptr = fast_ptr.next
        return head


def run():
    """
    Run
    """
    Solution.remove_nth_from_end(make_node([1, 2, 3, 4, 5]), 2).print_nodes()
    print("Remove nth node from list")
