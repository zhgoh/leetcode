#!/usr/bin/env python3
""" Reverse linked list """
from typing import Optional
from libs.ListNode import ListNode


class Solution:
    """
    Solution
    """
    @staticmethod
    def reverse_list(head: Optional[ListNode]) -> Optional[ListNode]:
        """
        Reverse a linked list
        """
        prev_node = None
        current_node = head

        while current_node:
            next_node = current_node.next
            current_node.next = prev_node
            prev_node = current_node
            current_node = next_node
        return prev_node

    @staticmethod
    def reverse_rec(prev, head):
        next_node = head.next
        head.next = prev

        if next_node:
            return Solution.reverse_rec(head, next_node)
        return head

    @staticmethod
    def reverse_list_rec(head: Optional[ListNode]) -> Optional[ListNode]:
        """
        Reverse a linked list rec
        """
        return Solution.reverse_rec(None, head)


def run():
    """
    Run
    """
    print("Reverse linked list done")
