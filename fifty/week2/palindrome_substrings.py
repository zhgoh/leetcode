#!/usr/bin/env python3
""" Maximum product subarray """


class Solution:
    """
    Solution
    """
    @staticmethod
    def is_palindrome(string: str) -> int:
        """
        Check if a string is a palindrome
        """
        str_len = len(string)
        mid_len = str_len // 2
        for i in range(mid_len):
            if string[i] != string[str_len - i - 1]:
                return 0
        return 1

    @staticmethod
    def count_substrings(string: str) -> int:
        """
        Count the substrings in a string
        """
        str_len = len(string)
        # Since single char themselves are each a palindrome
        res = str_len

        if str_len < 2:
            return res

        # Start checking for 2 chars or more
        left, right = 0, 1
        while True:
            if right == str_len:
                tmp = right - left
                left = 0
                right = left + tmp + 1

                if left == 0 and right == str_len:
                    break

            res += Solution.is_palindrome(string[left:right + 1])
            left += 1
            right += 1

        return res

    @staticmethod
    def count_substrings_fast(string: str) -> int:
        """
        Count substrings using neetcode method
        https://www.youtube.com/watch?v=4RACzI5-du8
        """
        res = 0
        str_len = len(string)

        if str_len < 2:
            return str_len

        # Find odd number palindrome
        mid = 0
        left, right = mid, mid
        while mid < str_len:
            if string[left] != string[right]:
                mid += 1
                left, right = mid, mid
                continue

            res += 1
            left -= 1
            right += 1

            if left < 0 or right >= str_len:
                mid += 1
                left, right = mid, mid

        # Find even number palindrome
        mid = 0
        left, right = mid, mid + 1
        while mid < str_len - 1:
            if string[left] != string[right]:
                mid += 1
                left, right = mid, mid + 1
                continue

            res += 1
            left -= 1
            right += 1

            if left < 0 or right >= str_len:
                mid += 1
                left, right = mid, mid + 1
        return res


def run():
    """
    Run
    """
    assert Solution.is_palindrome("a") == 1
    assert Solution.is_palindrome("aba") == 1
    assert Solution.is_palindrome("abba") == 1
    assert Solution.is_palindrome("") == 1
    assert Solution.is_palindrome("abc") == 0
    assert Solution.is_palindrome("abbc") == 0

    assert Solution.count_substrings("abc") == 3
    assert Solution.count_substrings("aaa") == 6
    assert Solution.count_substrings("aaaa") == 10
    assert Solution.count_substrings("abba") == 6
    assert Solution.count_substrings("abbba") == 9
    assert Solution.count_substrings("abcdefghhgfedcba") == 24

    assert Solution.count_substrings_fast("abc") == 3
    assert Solution.count_substrings_fast("aaa") == 6
    assert Solution.count_substrings_fast("aaaa") == 10
    assert Solution.count_substrings_fast("abba") == 6
    assert Solution.count_substrings_fast("abbba") == 9
    assert Solution.count_substrings_fast("abcdefghhgfedcba") == 24
    assert Solution.count_substrings_fast("fdsklf") == 6
    print("Palindrome substring done")
