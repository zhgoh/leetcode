#!/usr/bin/env python3
""" Linked List Cycle """
from typing import Optional
from libs.ListNode import ListNode


class Solution:
    """
    Solution
    """
    # Time: O(n)
    # Space: O(n)
    @staticmethod
    def has_cycle(head: Optional[ListNode]) -> bool:
        """
        Detect if a linked list has cycles
        """
        nodes = set()
        while head:
            if head in nodes:
                return True
            nodes.add(head)
            head = head.next
        return False

    @staticmethod
    def has_cycle_no_mem(head: Optional[ListNode]) -> bool:
        """
        Detect if a linked list , using O(1) space
        """
        fast = head
        slow = head

        while fast and fast.next:
            fast = fast.next.next
            slow = slow.next

            if fast == slow:
                return True
        return False


def run():
    """
    Run
    """
