#!/usr/bin/env python3
""" Number of islands """
from collections import deque


class Solution:
    """
    Solution
    """
    @staticmethod
    def bfs(grid: list[list[str]],
            pos: tuple[int, int]) -> set[tuple[int, int]]:
        """
        Breadth first search
        """
        visited: set[tuple[int, int]] = set()
        rows = len(grid)
        cols = len(grid[0])

        queue = deque([pos])
        dirs = ((-1, 0), (1, 0), (0, -1), (0, 1))

        while len(queue):
            visit = queue.popleft()

            if visit not in visited:
                visited.add(visit)

                for direction in dirs:
                    new_pos = (
                        visit[0] + direction[0],
                        visit[1] + direction[1])
                    if 0 <= new_pos[0] < rows and 0 <= new_pos[1] < cols:
                        if grid[new_pos[0]][new_pos[1]] == "1":
                            queue.append(new_pos)
        return visited

    @staticmethod
    def num_islands(grid: list[list[str]]) -> int:
        """
        Num of islands using BFS
        """
        res = 0
        rows = len(grid)

        if rows < 1:
            return res

        count = 0
        explored = set()
        cols = len(grid[0])

        for row in range(rows):
            for col in range(cols):
                if (row, col) in explored or grid[row][col] == "0":
                    continue

                count += 1
                explored.update(Solution.bfs(grid, (row, col)))
        return count

    @staticmethod
    def island(grid: list[list[str]], row: int, col: int):
        """
        Island
        """
        if grid[row][col] == "0":
            return

        grid[row][col] = "0"
        directions = ((0, -1), (0, 1), (-1, 0), (1, 0))
        for direction in directions:
            new_row = row + direction[0]
            new_col = col + direction[1]

            if (0 <= new_row < len(grid) and
                    0 <= new_col < len(grid[row])):
                Solution.island(grid, new_row, new_col)

    @staticmethod
    def num_islands_rec(grid: list[list[str]]) -> int:
        """
        Num of islands using BFS
        """
        rows = len(grid)
        cols = len(grid[0])

        count = 0

        for row in range(rows):
            for col in range(cols):
                if grid[row][col] == "1":
                    count += 1
                    Solution.island(grid, row, col)
        return count


def run():
    """
    Run
    """
    grid = [
        ["1", "1", "1", "1", "0"],
        ["1", "1", "0", "1", "0"],
        ["1", "1", "0", "0", "0"],
        ["0", "0", "0", "0", "0"]
    ]
    assert Solution.num_islands(grid) == 1
    assert Solution.num_islands_rec(grid) == 1
    grid = [
        ["1", "1", "0", "0", "0"],
        ["1", "1", "0", "0", "0"],
        ["0", "0", "1", "0", "0"],
        ["0", "0", "0", "1", "1"]
    ]
    assert Solution.num_islands(grid) == 3
    assert Solution.num_islands_rec(grid) == 3
    print("Number of island done")
