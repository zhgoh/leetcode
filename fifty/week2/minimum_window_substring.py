#!/usr/bin/env python3
""" Minimum window Substring """


class Solution:
    """
    Solution
    """
    @staticmethod
    def min_window(string: str, target: str) -> str:
        """
        Min windows
        """
        if target == "":
            return ""

        target_map: dict[str, int] = {}
        window_map: dict[str, int] = {}

        for char in target:
            target_map[char] = target_map.get(char, 0) + 1

        have, need = 0, len(target)
        res, res_len = (-1, -1), float("inf")
        str_len = len(string)
        left = 0

        for right in range(str_len):
            char = string[right]

            if char in target_map:
                window_map[char] = window_map.get(char, 0) + 1
                if window_map[char] <= target_map[char]:
                    have += 1

            while have == need:
                if (right - left + 1) < res_len:
                    res = (left, right)
                    res_len = right - left + 1

                char = string[left]
                if char in window_map:
                    window_map[char] -= 1

                    if window_map[char] < target_map[char]:
                        have -= 1
                left += 1
        left, right = res
        return string[left:right + 1] if res_len != float("inf") else ""


def run():
    """
    Run
    """
    assert Solution.min_window("ADOBECODEBANC", "ABC") == "BANC"
    assert Solution.min_window("a", "a") == "a"
    assert Solution.min_window("a", "aa") == ""
    assert Solution.min_window("aa", "aa") == "aa"
