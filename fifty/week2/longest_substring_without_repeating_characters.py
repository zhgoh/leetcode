#!/usr/bin/env python3
""" Longest substring """


class Solution:
    """
    Solution
    """
    # Time: O(n2)
    # Space: O(n)
    @staticmethod
    def length_of_longest_substring(string: str) -> int:
        """
        Get length of the longest substring in a string
        """
        max_char = 0
        str_len = len(string)

        for i in range(str_len):
            substring = set()
            substring.add(string[i])
            for j in range(i + 1, len(string)):
                if string[j] in substring:
                    break
                substring.add(string[j])
            max_char = max(max_char, len(substring))
        return max_char

    @staticmethod
    def length_of_longest_substring_fast(string: str) -> int:
        """
        Get the length of the longest substring
        """
        str_len = len(string)
        i = 0

        ans = 0
        chars: dict[str, int] = {}
        for j in range(str_len):
            tail = string[j]
            if tail in chars:
                i = max(i, chars[tail])
            ans = max(ans, j - i + 1)
            chars[tail] = j + 1
        return ans


def run():
    """
    Run
    """
    assert Solution.length_of_longest_substring_fast("abcabcbb") == 3
    assert Solution.length_of_longest_substring_fast("bbbbb") == 1
    assert Solution.length_of_longest_substring_fast("pwwkew") == 3
    assert Solution.length_of_longest_substring_fast("") == 0
    assert Solution.length_of_longest_substring_fast(" ") == 1
    assert Solution.length_of_longest_substring_fast("a") == 1
    assert Solution.length_of_longest_substring_fast("dvdf") == 3
    assert Solution.length_of_longest_substring_fast("abba") == 2
    print("Done")
