#!/usr/bin/env python3
""" Find minimum in rotated sorted array """


class Solution:
    """
    Solution
    """
    @staticmethod
    def find_min(nums: list[int]) -> int:
        """
        Find Min
        """
        left, right = 0, len(nums) - 1
        min_val = nums[0]

        while left <= right:
            mid = (left + right) // 2
            min_val = min(nums[left], nums[right], nums[mid], min_val)

            if nums[left] <= nums[mid]:
                # Left sorted portion
                if nums[left] > nums[right]:
                    left = mid + 1
                else:
                    right = mid - 1
            else:
                # Right sorted portion
                if nums[left] < nums[right]:
                    left = mid + 1
                else:
                    right = mid - 1
        return min_val


def run():
    """
    Run
    """
    assert Solution.find_min([3, 4, 5, 1, 2]) == 1
    assert Solution.find_min([4, 5, 6, 7, 0, 1, 2]) == 0
    assert Solution.find_min([11, 13, 15, 17]) == 11
    assert Solution.find_min([0, 0, 1, 1]) == 0
    assert Solution.find_min([6, 0, 1, 2, 3, 4, 5]) == 0
    assert Solution.find_min([3, 4, 5, 6, 1, 2]) == 1
    print("Find min in rotated sorted array done")
