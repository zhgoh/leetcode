#!/usr/bin/env python3
""" Container with most water """


class Solution:
    """
    Solution
    """
    @staticmethod
    def max_area(height: list[int]) -> int:
        """
        Find the max area of water in the container
        """
        max_area = 0
        height_len = len(height)
        for i in range(height_len):
            p1 = height[i]
            for j in range(i + 1, height_len):
                p2 = height[j]
                # min of two pillar * width
                h = min(p1, p2)
                w = j - i
                max_area = max(max_area, h * w)
        return max_area

    @staticmethod
    def max_area_fast(height: list[int]) -> int:
        """
        Faster way to solve
        """
        height_len = len(height)
        left, right = 0, height_len - 1
        max_area = 0

        while True:
            if left == right:
                break
            max_area = max(max_area, min(height[left], height[right]) * (right - left))

            if height[left] < height[right]:
                left += 1
            else:
                right -= 1
        return max_area


def run():
    """
    Run tests
    """
    assert Solution.max_area([1, 1]) == 1
    assert Solution.max_area([1, 8, 6, 2, 5, 4, 8, 3, 7]) == 49

    assert Solution.max_area_fast([1, 1]) == 1
    assert Solution.max_area_fast([1, 8, 6, 2, 5, 4, 8, 3, 7]) == 49
    assert Solution.max_area_fast([1, 2, 1]) == 2
    assert Solution.max_area_fast([1, 8, 6, 2, 5, 4, 8, 25, 7]) == 49
    print("Done")
