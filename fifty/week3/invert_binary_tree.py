#!/usr/bin/env python3
""" Maximum product subarray """
from typing import Optional


class TreeNode:
    """
    Tree node from leetcode
    """
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    """
    Solution
    """
    @staticmethod
    def invert_tree(root: Optional[TreeNode]) -> Optional[TreeNode]:
        """
        Invert tree
        """

        if root is None:
            return None

        nodes: list[TreeNode] = [root]
        while len(nodes) > 0:
            node = nodes.pop()
            if node.left is not None:
                nodes.append(node.left)
            if node.right is not None:
                nodes.append(node.right)
            node.left, node.right = node.right, node.left
        return root


def run():
    """
    Run
    """
    print("Invert binary tree done")
