#!/usr/bin/env python3
""" Palindrome number """


class Solution:
    """
    Solution
    """
    # Time: O(n)
    # Space: O(n)
    @staticmethod
    def is_palindrome(num: int) -> bool:
        """
        Check if number is palindrome
        """
        string = str(num)
        num_len = len(string)
        mid = num_len >> 1

        for i in range(mid):
            if string[i] != string[num_len - i - 1]:
                return False
        return True

    @staticmethod
    def is_palindrome_correct(num: int) -> bool:
        """
        Based on leetcode answers, without using string
        """
        if num < 0 or num % 10 == 0 and num != 0:
            return False

        reverse = 0
        while reverse < num:
            reverse = 10 * reverse + num % 10
            num //= 10

        return num in (reverse, reverse // 10)


def run():
    """
    Run tests
    """
    assert Solution.is_palindrome(121)
    assert not Solution.is_palindrome(-121)
    assert not Solution.is_palindrome(10)
    assert Solution.is_palindrome(0)

    assert Solution.is_palindrome_correct(121)
    assert Solution.is_palindrome_correct(0)
    assert not Solution.is_palindrome_correct(-121)
    assert not Solution.is_palindrome_correct(10)
    print("Done")
