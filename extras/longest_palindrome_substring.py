#!/usr/bin/env python3
""" Longest palindrome substring """


class Solution:
    """
    Solution
    """
    # Time: O(n3)
    # Space: O(1)
    @staticmethod
    def longest_palindrome(string: str) -> str:
        """
        Get the longest Palindrome
        """
        str_len = len(string)
        max_len = 0
        longest = ""
        for i in range(str_len):
            for j in range(i + 1, str_len + 1):
                if Solution.is_palindrome(string[i:j]):
                    if (j - i) > max_len:
                        max_len = j - i
                        longest = string[i:j]
        return longest

    @staticmethod
    def is_palindrome(string: str) -> bool:
        """
        Check if a string is palindrome
        """
        str_len = len(string)
        mid = str_len >> 1

        for i in range(mid):
            if string[i] != string[str_len - i - 1]:
                return False
        return True

    @staticmethod
    def helper_mid(string: str, str_len: int,
                   left: int, right: int,
                   longest: str, longest_len: int) -> tuple[str, int]:
        while (left >= 0 and right < str_len
               and string[left] == string[right]):
            if (right - left + 1) > longest_len:
                longest = string[left:right + 1]
                longest_len = right - left + 1
            left -= 1
            right += 1
        return longest, longest_len

    # Time: O(n2)
    # Space: O(1)
    @staticmethod
    def longest_palindrome_mid(string: str) -> str:
        """
        Get the longest Palindrome, using mid-point checks
        """
        str_len = len(string)
        longest = ""
        longest_len = 0

        for i in range(str_len):
            # Odd number palindrome
            longest, longest_len = Solution.helper_mid(
                string, str_len, i, i, longest, longest_len)
            longest, longest_len = Solution.helper_mid(
                string, str_len, i, i + 1, longest, longest_len)
        return longest


def run():
    """
    run
    """
    assert Solution.is_palindrome("babab")
    assert Solution.is_palindrome("abba")
    assert Solution.is_palindrome("a")
    assert Solution.is_palindrome("")
    assert not Solution.is_palindrome("baba")
    assert not Solution.is_palindrome("abcd")

    assert Solution.longest_palindrome("babab") == "babab"
    assert Solution.longest_palindrome("") == ""
    assert Solution.longest_palindrome("a") == "a"
    assert Solution.longest_palindrome("abcd") == "a"
    assert Solution.longest_palindrome("babad") == "bab"
    assert Solution.longest_palindrome("cbbd") == "bb"

    assert Solution.longest_palindrome_mid("babab") == "babab"
    assert Solution.longest_palindrome_mid("") == ""
    assert Solution.longest_palindrome_mid("a") == "a"
    assert Solution.longest_palindrome_mid("abcd") == "a"
    assert Solution.longest_palindrome_mid("babad") == "bab"
    assert Solution.longest_palindrome_mid("cbbd") == "bb"

    # assert Solution.longest_palindrome("babad") == "aba"
    # assert Solution.longest_palindrome("cbbd") == "bb"
    # assert Solution.longest_palindrome("a") == "a"
    # assert Solution.longest_palindrome("") == ""
    # assert Solution.longest_palindrome("ab") == "a"
