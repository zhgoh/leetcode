#!/usr/bin/env python3
""" Add two numbers """
from libs.ListNode import ListNode, make_node, get_len


class Solution:
    """
    Solution
    """
    @staticmethod
    def add_two_numbers_list(l1_list: list, l2_list: list) -> list:
        """
        Add two numbers from a list
        """
        l1_len = len(l1_list)
        l2_len = len(l2_list)
        longer = max(l1_len, l2_len)

        res = []
        carry = 0
        for i in range(longer):
            top, bot = 0, 0
            if i < l1_len:
                top = l1_list[i]
            if i < l2_len:
                bot = l2_list[i]

            new_sum = top + bot + carry
            carry = 0

            if new_sum > 9:
                carry = 1
                new_sum -= 10
            res.append(new_sum)
        # Remember to add carry
        if carry:
            res.append(1)
        return res

    @staticmethod
    def add_two_numbers_node(l1_node: ListNode, l2_node: ListNode) -> ListNode:
        """
        Add two numbers from the two list
        :param l1_node
        :param l2_node
        :return:
        """
        # Time:
        # Space:

        l1_len = get_len(l1_node)
        l2_len = get_len(l2_node)
        longer = max(l1_len, l2_len)

        head = ListNode()
        res = head

        carry = 0
        top_node, bot_node = l1_node, l2_node
        for _ in range(longer):
            if top_node is None:
                top = 0
            else:
                top = top_node.val
                top_node = top_node.next

            if bot_node is None:
                bot = 0
            else:
                bot = bot_node.val
                bot_node = bot_node.next

            new_sum = top + bot + carry
            carry = 0

            if new_sum > 9:
                carry = 1
                new_sum -= 10
            res.next = ListNode(new_sum)
            res = res.next

        if carry:
            res.next = ListNode(1)
        return head.next


def run():
    """
    Run our tests
    """
    assert Solution.add_two_numbers_list([2, 4, 3], [5, 6, 4]) == [7, 0, 8]
    assert Solution.add_two_numbers_list(
        [9, 9, 9, 9, 9, 9, 9], [9, 9, 9, 9]) == [8, 9, 9, 9, 0, 0, 0, 1]
    assert Solution.add_two_numbers_list([0], [0]) == [0]

    Solution.add_two_numbers_node(
        make_node([2, 4, 3]), make_node([5, 6, 4])).print_nodes()
    Solution.add_two_numbers_node(
        make_node([2, 4, 3]), make_node([5, 6, 4])).print_nodes()
    Solution.add_two_numbers_node(
        make_node([2, 4, 3]), make_node([5, 6, 4])).print_nodes()

    # No asserts as I did not implement eq operator
    # assert(
    #     sol.addTwoNumbers_node(
    #         make_linked([2,4,3]),
    #         make_linked([5,6,4])) ==
    #         make_linked([7,0,8]))
    # assert(
    #     sol.addTwoNumbers_node(
    #         make_linked([9,9,9,9,9,9,9]),
    #         make_linked([9,9,9,9])) ==
    #         make_linked([8,9,9,9,0,0,0,1]))
    # assert(
    #     sol.addTwoNumbers_node(
    #         make_linked([0]),
    #         make_linked([0])) ==
    #         make_linked([0]))
