#!/usr/bin/env python3

class Solution:
    """
    Solutions
    """
    @staticmethod
    def roman_to_int(string: str) -> int:
        """
        Roman to int
        """
        # I can be before V(5) and X(10) to make 4 and 9
        # X can be placed before L(50) and C(100) to make 40 and 90
        # C can be placed before D(500) and M(1000) to make 400 and 900
        res = 0
        numerals = {
            "M": 1000,
            "D": 500,
            "C": 100,
            "L": 50,
            "X": 10,
            "V": 5,
            "I": 1
        }

        prev = 1000

        for char in string:
            num = numerals[char]
            if prev < num:
                res -= (prev << 1)
            res += num
            prev = num
        print(res)
        return res


def run():
    """
    Run the test
    """
    assert Solution.roman_to_int("III") == 3
    assert Solution.roman_to_int("LVIII") == 58
    assert Solution.roman_to_int("MCMXCIV") == 1994
