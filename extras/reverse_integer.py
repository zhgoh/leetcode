#!/usr/bin/env python3
""" Reverse integer """


class Solution:
    """
    Solution
    """
    @staticmethod
    def reverse(num: int) -> int:
        """
        Reverse integer
        """
        max_signed = 2 ** 31
        neg = bool(num < 0)
        num = abs(num)
        res = int(str(num)[::-1].strip())
        if neg:
            res = -res

        if res < -max_signed or res > (max_signed - 1):
            res = 0
        return res

    # Code: https://www.code-recipe.com/post/reverse-integer
    @staticmethod
    def reverse_proper(num: int) -> int:
        """
        Using proper method as described in leetcode
        """
        max_signed = 2 ** 31
        sign = 1
        res = 0
        if num < 0:
            sign = -1
            num *= sign

        while num > 0:
            res = res * 10 + (num % 10)
            if (res * sign) < -max_signed or (res * sign) > (max_signed - 1):
                return 0
            num //= 10
        return sign * res


def run():
    """
    Run tests
    """
    assert Solution.reverse(123) == 321
    assert Solution.reverse(-123) == -321
    assert Solution.reverse(120) == 21
    assert Solution.reverse(0) == 0
    assert Solution.reverse(200) == 2
    assert Solution.reverse(-200) == -2
    assert Solution.reverse(1534236469) == 0

    assert Solution.reverse_proper(123) == 321
    assert Solution.reverse_proper(-123) == -321
    assert Solution.reverse_proper(120) == 21
    assert Solution.reverse_proper(0) == 0
    assert Solution.reverse_proper(200) == 2
    assert Solution.reverse_proper(-200) == -2
    assert Solution.reverse_proper(1534236469) == 0
