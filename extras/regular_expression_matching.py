#!/usr/bin/env python3

class Solution:
    """
    Solutions
    """
    # Time:
    # Space:
    @staticmethod
    def is_match_dp(string: str, pattern: str) -> bool:
        """
        Find whether a word is a match
        Solution: https://hecodesit.com/regular-expression-matching-leetcode-python-solution/
        """
        rows = len(string) + 1
        cols = len(pattern) + 1

        # Just save on computation, just terminate early
        if cols == 0:
            if rows == 0:
                return True
            return False

        #   _ a * a
        # _ F F F F
        # a F F F F
        # a F F F F
        # Array init to row + 1 by col + 1 (taking empty pattern and string
        # into consideration)
        dp = [[False for _ in range(cols)] for _ in range(rows)]

        #   _ a * a
        # _ T F F F
        # a F F F F
        # a F F F F
        dp[0][0] = True

        #   _ a * a
        # _ T F T F
        # a F F F F
        # a F F F F
        # Fill up first row, looking for any star char
        # if pattern is a star, look at two chars before
        for i in range(2, cols):
            if pattern[i - 1] == "*":
                dp[0][i] = dp[0][i - 2]

        # Fill up rest of the arrays
        for r in range(1, rows):
            for c in range(1, cols):
                # Match or matches .
                if string[r - 1] == pattern[c - 1] or pattern[c - 1] == '.':
                    dp[r][c] = dp[r - 1][c - 1]
                elif c > 1 and pattern[c - 1] == "*":
                    dp[r][c] = dp[r][c - 2]
                    if pattern[c - 2] == '.' or pattern[c - 2] == string[r - 1]:
                        dp[r][c] = dp[r][c] or dp[r - 1][c]
        return dp[rows - 1][cols - 1]

    @staticmethod
    def is_match_dfs(string: str, pattern: str) -> bool:
        """
        Using dfs, from https://www.youtube.com/watch?v=HAA8mgxlov8
        """
        str_len = len(string)
        pat_len = len(pattern)

        cache = {}

        def dfs(i, j):
            """
            Depth first search
            """
            if (i, j) in cache:
                return cache[(i, j)]
            # Base case
            # when i > str_len, j > pat_len, matched
            # only j > pat_len, out of bounds
            if i >= str_len and j >= pat_len:
                return True
            if j >= pat_len:
                return False

            # i could be out of bounds, handle it here
            match = i < str_len and (string[i] == pattern[j] or pattern[j] == ".")

            # Checking for * first
            # Decisions
            # if we use *
            # (i + 1, j)
            # else we do not use *
            # (i, j + 2)
            if (j + 1) < pat_len and pattern[j + 1] == "*":
                cache[(i, j)] = (dfs(i, j + 2) or            # Don't use *
                                 (match and dfs(i + 1, j)))  # Use *
                return cache[(i, j)]
            if match:
                cache[(i, j)] = dfs(i + 1, j + 1)
                return cache[(i, j)]
            cache[(i, j)] = False
            return False
        return dfs(0, 0)


def run():
    """
    Run
    """
    assert not Solution.is_match_dp("aa", "a")
    assert Solution.is_match_dp("aa", "a*")
    assert Solution.is_match_dp("aab", "a*b")
    assert Solution.is_match_dp("ab", ".*")
    assert Solution.is_match_dp("ab", "..")
    assert Solution.is_match_dp("aabc", "a*b.")
    assert Solution.is_match_dp("aaa", "a*a")
    assert not Solution.is_match_dp("aabc", "a*b")
    assert not Solution.is_match_dp("ab", ".*c")
    assert not Solution.is_match_dp("aaa", "aaaa")
    assert Solution.is_match_dp("", ".*")
