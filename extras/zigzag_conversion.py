#!/usr/bin/env python3
""" Zig zag conversion """


class Solution:
    """
    Solution
    """
    # Time: O(n2)
    # Space: O(n)
    @staticmethod
    def convert(string: str, num_rows: int) -> str:
        """
        Convert
        """
        res = ""
        str_len = len(string)
        gap = 2 if num_rows > 1 else 1

        for row in range(num_rows):
            # First and last row can increment wide
            if row in (0, (num_rows - 1)):
                # Only for top and bottom row
                increment = (num_rows + (num_rows - gap))

                i = row
                while i < str_len:
                    res += string[i]
                    i += increment
            else:
                i = row
                increment_1 = (num_rows - row - 1) << 1
                increment_2 = i << 1
                pillar = True

                while i < str_len:
                    res += string[i]
                    if pillar:
                        i += increment_1
                    else:
                        i += increment_2
                    pillar = not pillar
        return res


def run():
    """ Run """
    assert Solution.convert("123456", 3) == "152463"
    assert Solution.convert("PAYPALISHIRING", 3) == "PAHNAPLSIIGYIR"
    assert Solution.convert("PAYPALISHIRING", 4) == "PINALSIGYAHRPI"
    assert Solution.convert("A", 1) == "A"
