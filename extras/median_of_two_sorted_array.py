#!/usr/bin/env python3
# Solutions from: https://www.youtube.com/watch?v=q6IEA26hvXc
""" Median of two sorted array """


def find_median_sorted_arrays(nums1: list[int], nums2: list[int]) -> float:
    """
    Find the median of two sorted arrays
    """
    # Make sure a_num is smaller
    a_num, b_num = nums1, nums2
    total = len(a_num) + len(b_num)
    half = total // 2
    if len(b_num) < len(a_num):
        a_num, b_num = b_num, a_num

    left, r = 0, len(a_num) - 1
    while True:
        i = (left + r) // 2  # a_num
        j = half - i - 2  # b_num

        a_left = a_num[i] if i >= 0 else float("-inf")
        a_right = a_num[i + 1] if (i + 1) < len(a_num) else float("inf")
        b_left = b_num[j] if j >= 0 else float("-inf")
        b_right = b_num[j + 1] if (j + 1) < len(b_num) else float("inf")

        if a_left <= b_right and b_left <= a_right:
            # odd
            if total % 2:
                return min(a_right, b_right)
            # even
            return (max(a_left, b_left) + min(a_right, b_right)) / 2
        elif a_left > b_right:
            r = i - 1
        else:
            left = i + 1


def find_median_sorted_arrays_fast(nums1, nums2) -> float:
    total = len(nums1) + len(nums2)
    median = 0.0
    i, j = 0, 0
    if_even = False

    if total % 2:
        mid = total // 2 + 1
    else:
        mid = total // 2
        if_even = True

    for itr in range(mid):
        if i >= len(nums1):
            median = nums2[j]
            j += 1
            continue
        elif j >= len(nums2):
            median = nums1[i]
            i += 1
            continue

        if nums1[i] < nums2[j]:
            median = nums1[i]
            i += 1
        elif nums2[j] < nums1[i]:
            median = nums2[j]
            j += 1
        else:
            median = nums1[i]
            i += 1

    if if_even:
        if i >= len(nums1):
            next_val = nums2[j]
        elif j >= len(nums2):
            next_val = nums1[i]
        else:
            next_val = min(nums1[i], nums2[j])
        median = (median + next_val) / 2
    return median


def test():
    assert (find_median_sorted_arrays([1, 3], [2]) == 2.0)
    assert (find_median_sorted_arrays([1, 3], [2]) == 2.5)


test()
