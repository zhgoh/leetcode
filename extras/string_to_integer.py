#!/usr/bin/env python3
""" String to integer (atoi)"""


class Solution:
    """
    Solution
    """
    @staticmethod
    def my_atoi(string: str) -> int:
        """
        myAtoi
        """

        string = string.strip()
        if len(string) == 0:
            return 0

        res = 0
        sign = 1
        if string[0] == '-':
            sign = -1
            string = string[1:]
        elif string[0] == '+':
            sign = 1
            string = string[1:]

        for char in string:
            if '0' <= char <= '9':
                res = res * 10 + (ord(char) - ord('0'))
            else:
                break
        res *= sign
        max_int = 2 ** 31

        if res < -max_int:
            res = -max_int
        elif res > (max_int - 1):
            res = max_int - 1
        return res


def run():
    """
    Run
    """
    assert Solution.my_atoi("42") == 42
    assert Solution.my_atoi("    -42") == -42
    assert Solution.my_atoi("4193 with words") == 4193
    assert Solution.my_atoi("with words 987") == 0
    assert Solution.my_atoi("493abc") == 493
    assert Solution.my_atoi("43-") == 43
    assert Solution.my_atoi("3-1") == 3
    assert Solution.my_atoi("+1") == 1
    assert Solution.my_atoi("21474836460") == 2147483647
    print("Done")
